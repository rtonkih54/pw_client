import axios from "axios";

export default {
  user: {
    login: credentials =>
      axios.post("/api/login", { ...credentials }).then(res => res.data.success),
    signup: user =>
      axios.post("/api/register", { ...user }).then(res => res.data.success),
    sendPW: data =>
      axios.post("api/transactions", { ...data }).then(res => {
        return res.data.success.user.balance;
      })
  }
};
