import api from "../api";
import { userLoggedIn } from "./auth";
import { PW_SENT } from "../types";

export const signup = data => dispatch =>
  api.user.signup(data).then(user => {
    localStorage.bookwormJWT = user.token;
    dispatch(userLoggedIn(user));
  });

export const pwSent = (balance) => ({
  type: PW_SENT,
  balance
});

export const sendPW = data => dispatch =>
  api.user.sendPW(data).then(balance => {
    dispatch(pwSent(balance));
    localStorage.balance = balance;
    return balance;
  });
