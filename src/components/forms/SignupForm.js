import React from "react";
import PropTypes from "prop-types";
import { Form, Button } from "semantic-ui-react";
import isEmail from "validator/lib/isEmail";
import InlineError from "../messages/InlineError";

class SignupForm extends React.Component {
  state = {
    data: {
      email: "",
      first_name: "",
      last_name: "",
      password: "",
      c_password: ""
    },
    loading: false,
    errors: {}
  };

  onChange = e =>
    this.setState({
      ...this.state,
      data: { ...this.state.data, [e.target.name]: e.target.value }
    });

  onSubmit = e => {
    e.preventDefault();
    const errors = this.validate(this.state.data);
    this.setState({ errors });
    if (Object.keys(errors).length === 0) {
      this.setState({ loading: true });
      this.props
        .submit(this.state.data)
        .catch(err =>
          this.setState({ errors: err.response.data.errors, loading: false })
        );
    }
  };

  validate = data => {
    const errors = {};

    if (!isEmail(data.email)) errors.email = "Invalid email";
    if (!data.first_name) errors.first_name = "Can't be blank";
    if (!data.last_name) errors.last_name = "Can't be blank";
    if (!data.password) errors.password = "Can't be blank";
    if (data.password != data.c_password) errors.c_password = "Password doesnt match confirm password";

    return errors;
  };

  render() {
    const { data, errors, loading } = this.state;

    return (
      <Form onSubmit={this.onSubmit} loading={loading}>
        <Form.Field error={!!errors.email}>
          <label htmlFor="email">Email</label>
          <input
            type="email"
            id="email"
            name="email"
            placeholder="email@email.com"
            value={data.email}
            onChange={this.onChange}
          />
          {errors.email && <InlineError text={errors.email} />}
        </Form.Field>

        <Form.Field error={!!errors.first_name}>
          <label htmlFor="first_name">Firstname</label>
          <input
            type="text"
            id="first_name"
            name="first_name"
            placeholder="John"
            value={data.first_name}
            onChange={this.onChange}
          />
          {errors.first_name && <InlineError text={errors.first_name} />}
        </Form.Field>

        <Form.Field error={!!errors.last_name}>
          <label htmlFor="last_name">Lastname</label>
          <input
            type="text"
            id="last_name"
            name="last_name"
            placeholder="Doe"
            value={data.last_name}
            onChange={this.onChange}
          />
          {errors.last_name && <InlineError text={errors.last_name} />}
        </Form.Field>

        <Form.Field error={!!errors.password}>
          <label htmlFor="password">Password</label>
          <input
            type="password"
            id="password"
            name="password"
            value={data.password}
            onChange={this.onChange}
          />
          {errors.password && <InlineError text={errors.password} />}
        </Form.Field>

        <Form.Field error={!!errors.c_password}>
          <label htmlFor="c_password">Confirm Password</label>
          <input
            type="password"
            id="c_password"
            name="c_password"
            value={data.c_password}
            onChange={this.onChange}
          />
          {errors.c_password && <InlineError text={errors.c_password} />}
        </Form.Field>

        <Button primary>Sign Up</Button>
      </Form>
    );
  }
}

SignupForm.propTypes = {
  submit: PropTypes.func.isRequired
};

export default SignupForm;
