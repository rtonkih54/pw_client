import React from "react";
import PropTypes from "prop-types";
import axios from "axios";
import { Form, Dropdown } from "semantic-ui-react";

class SearchUserForm extends React.Component {
  state = {
    query: "",
    loading: false,
    options: [],
    users: {}
  };

  onSearchChange = (e, { searchQuery }) => {
    clearTimeout(this.timer);
    this.setState({
      query: searchQuery
    });
    this.timer = setTimeout(this.fetchOptions, 1000);
  };

  onChange = (e, data) => {
    this.setState({ query: data.value });
    this.props.onUserSelect(this.state.users[data.value]);
  };

  fetchOptions = () => {
    if (!this.state.query) return;
    this.setState({ loading: true });
    axios
      .get(`/api/users?q=${this.state.query}`)
      .then(res => res.data.users)
      .then(users => {
        const options = [];
        const usersHash = {};
        users.forEach(user => {
          usersHash[user.id] = user;
          options.push({
            key: user.id,
            value: user.id,
            text: user.first_name + " " + user.last_name
          });
        });
        this.setState({ loading: false, options, users: usersHash });
      });
  };

  render() {
    return (
      <Form>
        <Dropdown
          search
          fluid
          placeholder="Search for a user by firstname"
          value={this.state.query}
          onSearchChange={this.onSearchChange}
          options={this.state.options}
          loading={this.state.loading}
          onChange={this.onChange}
        />
      </Form>
    );
  }
};

SearchUserForm.propTypes = {
  onUserSelect: PropTypes.func.isRequired
};

export default SearchUserForm;
