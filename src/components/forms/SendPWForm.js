import React from "react";
import PropTypes from "prop-types";
import { Form, Button, Grid, Segment, Image, Message } from "semantic-ui-react";
import InlineError from "../messages/InlineError";

class SendPWForm extends React.Component {
  state = {
    user: {
      id: this.props.user.id,
      full_name: this.props.user.first_name + " " + this.props.user.last_name,
      balance: this.props.user.balance,
      send_balance: ""
    },
    loading: false,
    errors: {}
  };

  onSubmit = e => {
    e.preventDefault();
    const errors = this.validate(this.state.user);
    this.setState( { errors });
    if (Object.keys(errors).length === 0) {
      this.setState({ loading: true });
      this.props
        .submit(this.state.user)
        .then(this.setState({ loading: false }))
        .catch(err =>
          this.setState({ errors: { global: err.response.data.errors.global }, loading: false })
        );
    }
  };

  componentWillReceiveProps(props) {
      if (this.state.user.id !== props.user.id) {
          this.setState({
              ...this.state,
              user: { ...this.state.user,
                 id: props.user.id,
                 full_name: props.user.first_name + " " + props.user.last_name
                 }
          });
      }
  }

  onChange = e =>
   this.setState({
     ...this.state,
     user: { ...this.state.user, [e.target.name]: e.target.value }
   });

  validate = user => {
   const errors = {};
   if (!user.full_name) errors.full_name = "Can't be blank";
   if (!user.send_balance) errors.send_balance = "Can't be blank";
   if (parseInt(user.send_balance) <= 0) {
     errors.wront_amount = "You cannot send that amout of PW";
   }
   return errors;
  };

  render() {
    const { errors, user, loading } = this.state;

    return (
      <Segment>
        <Form onSubmit={this.onSubmit} loading={loading}>
          {errors.global && (
            <Message negative>
              <Message.Header>Something went wrong</Message.Header>
              <p>{errors.global}</p>
            </Message>
          )}
          <Grid columns={2} stackable>
            <Grid.Row>
              <Grid.Column>
                <Form.Field error={!!errors.full_name}>
                  <label htmlFor="full_name">User Firstname</label>
                  <input
                    type="text"
                    id="full_name"
                    name="full_name"
                    placeholder="Firstname"
                    value={user.full_name}
                    onChange={this.onChange}
                  />
                  {errors.full_name && <InlineError text={errors.full_name} />}
                </Form.Field>

                <Form.Field error={!!errors.send_balance}>
                  <label htmlFor="balance">Balance to send</label>
                  <input
                    type="text"
                    id="send_balance"
                    name="send_balance"
                    placeholder="How many PW send?"
                    value={user.send_balance}
                    onChange={this.onChange}
                  />
                  {errors.send_balance && <InlineError text={errors.send_balance} />}
                  {errors.wront_amount && <InlineError text={errors.wront_amount} />}
                </Form.Field>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Button primary>Send</Button>
            </Grid.Row>
          </Grid>
        </Form>
      </Segment>
    );
  }
}

SendPWForm.propTypes = {
  submit: PropTypes.func.isRequired
};

export default SendPWForm;
