import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Segment } from "semantic-ui-react";
import axios from "axios";
import SearchUserForm from "../forms/SearchUserForm";
import SendPWForm from '../forms/SendPWForm';
import { sendPW } from "../../actions/users";

class SendPWPage extends React.Component {
  state = {
    user: null
  };

  onUserSelect = user => {
    this.setState({ user })
  };

  sendPW = data =>
    this.props.sendPW(data).then(res => {
      this.setState({ res });
    });

  render() {
      return (
      <Segment>
        <h1>Choose user to transfer PW</h1>
        <SearchUserForm onUserSelect={this.onUserSelect}/>

        {this.state.user &&
          <SendPWForm submit={this.sendPW} user={this.state.user}/>}
      </Segment>
    );
  }
}

export default connect(null, { sendPW })(SendPWPage);
