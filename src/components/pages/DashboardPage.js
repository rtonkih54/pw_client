import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

class DashboardPage extends React.Component {

  render() {
    const { isAuthenticated } = this.props;
    return (
      <div>
      </div>
    );
  }
}

DashboardPage.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired
};

function mapStateToProps(state) {
  return {
    isAuthenticated: !!state.user.token
  };
}

export default connect(mapStateToProps)(DashboardPage);
