import React from "react";
import PropTypes from "prop-types";
import { Segment } from "semantic-ui-react";
import ReactTable from 'react-table'
import "react-table/react-table.css";
import axios from 'axios';
import setAuthorizationHeader from "../../utils/setAuthorizationHeader";

class TransactionsPage extends React.Component {
  state = {
    transactions: []
  };

  componentWillMount() {
    setAuthorizationHeader(localStorage.bookwormJWT);
    axios
      .get(`/api/all_transactions`).then(res => {
        console.log(res.data.success.transactions.data);
        const transactions = res.data.success.transactions.data;
        this.setState({ transactions: transactions })
      });
  }

  render() {
    const data = this.state.transactions;

    const columns = [{
        Header: 'Time',
        accessor: 'created_at'
      }, {
        Header: 'From',
        accessor: 'sender_name' // String-based value accessors!
      }, {
        Header: 'Recipient',
        accessor: 'recipient_name',
      }, {
        Header: 'Balance before',
        accessor: 'balance_before'
      }, {
        Header: 'Balance after',
        accessor: 'balance_after'
      }]

      return (
      <Segment>
        <h1>Recent transactions</h1>
        <div>
         <ReactTable
           data={data}
           columns={columns}
           defaultPageSize={10}
           className="-striped -highlight"
         />
         <br />
       </div>
      </Segment>
    );
  }
}

export default TransactionsPage;
